<?php

class BienesModel extends Mysql{

    public function __construct()
    {
        parent::__construct();
    }

    public function setBienes(string $direccion, string $ciudad, string $telefono, string $codigo_postal, string $tipo, float $precio)
    {
        $query_insert ="INSERT INTO bienes(direccion,ciudad,telefono,codigo_postal,tipo,precio) VALUES(?,?,?,?,?,?)";
        $arrData = array($direccion,$ciudad,$telefono,$codigo_postal,$tipo,$precio);
        $request_insert = $this->insert($query_insert,$arrData);
        return $request_insert;
    }

    public function deleteBien($id)
    {
        $sql = "DELETE FROM bienes WHERE id_bienes = $id";
        $request = $this->delete($sql);
        return $request;
    }
}



?>