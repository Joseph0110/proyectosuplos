<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="resource/css/materialize.min.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="resource/css/customColors.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="resource/css/ion.rangeSlider.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="resource/css/ion.rangeSlider.skinFlat.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="resource/css/index.css"  media="screen,projection"/>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"> -->
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Formulario</title>
</head>

<body>
  <video src="img/video.mp4" id="vidFondo"></video> 
<!-- <pre> -->
<?php 
    // print_r($data['misBienes']);
?>
 <!-- <button type="button"  data-bs-toggle="modal" data-bs-target="#exampleModal">
      Launch demo modal
    </button> -->
<div class="contenedor">
    <div class="card rowTitulo">
      <h1>Bienes Intelcost</h1>
    </div>
     <!-- Button trigger modal -->
    
    <div class="colFiltros">
      <!-- <form action="<?php echo BASE_URL."Bienes/buscar"?>" method="post" id="formulario"> -->
        <div class="filtrosContenido">
          <div class="tituloFiltros">
            <h5>Filtros</h5>
          </div>
          <div class="filtroCiudad input-field">
            <p><label for="selectCiudad">Ciudad:</label><br></p>
            <select name="ciudad" id="selectCiudad">
            <option value="">[Elige una ciudad]</option>
                <?php foreach ($data['ciudades'] as $key => $ciudad):?>
                    <option value="<?php echo $ciudad?>" selected><?php echo $ciudad?></option>
                <?php endforeach?>
            </select>
          </div>
          <div class="filtroTipo input-field">
            <p><label for="selecTipo">Tipo:</label></p>
            <br>
            <select name="tipo" id="selectTipo">
              <option value="">[Elige un tipo]</option>
              <?php foreach ($data['tipos'] as $key => $tipo):?>
                    <option value="<?php echo $tipo?>" selected><?php echo $tipo?></option>
                <?php endforeach?>
            </select>
          </div>
          <div class="filtroPrecio">
            <label for="rangoPrecio">Precio:</label>
            <input type="text" id="rangoPrecio" name="precio" value="" />
          </div>
          <div class="botonField">
            <!-- <input type="submit" class="btn white" value="Buscar" id="submitButton"> -->
            <button class="btn " id="submitButton" onclick="buscarBienes()">Buscar</button>
          </div>
        </div>
      <!-- </form> -->
    </div>
    <div id="tabs" style="width: 75%;">
      <ul>
        <li><a href="#tabs-1">Bienes disponibles</a></li>
        <li><a href="#tabs-2">Mis bienes</a></li>
        <li><a href="#tabs-3">Reportes</a></li>
      </ul>
      <div id="tabs-1">
        <div class="colContenido" id="divResultadosBusqueda">
          <div class="tituloContenido card" style="justify-content: center;">
            <h5>Resultados de la búsqueda:</h5>
            <?php foreach ($data['bienes'] as $key => $bien) :?>     
                <div class="divider row">
                    <div class="imagenBien">
                        <img src="./resource/img/home.jpg" alt="" width="200px" height="200px">
                    </div>
                    <div class="datosBien">
                        <!-- <form action="<?php echo BASE_URL."Bienes/Store"?>" method="POST"> -->
                            Dirección: <?php echo $bien->Direccion?><br>
                            <input type="hidden" name="direccion" id="direccion" value="<?php echo $bien->Direccion?>">
                            Ciudad: <?php echo $bien->Ciudad?><br>
                            <input type="hidden" name="ciudad" id="ciudad"  value="<?php echo $bien->Ciudad?>">
                            Telefono: <?php echo $bien->Telefono?><br>
                            <input type="hidden" name="telefono" id="telefono"  value="<?php echo $bien->Telefono?>">
                            Codigo Postal: <?php echo $bien->Codigo_Postal?><br>
                            <input type="hidden" name="codigoPosta" id="codigoPosta"  value="<?php echo $bien->Codigo_Postal?>">
                            Tipo: <?php echo $bien->Tipo?><br>
                            <input type="hidden" name="tipo" id="tipo"  value="<?php echo $bien->Tipo?>">
                            Precio: <?php echo $bien->Precio?><br>
                            <input type="hidden" name="precio" id="precio" value="<?php echo $bien->Precio?>"> 
                            <!-- <button type="submit"> Guardar</button> -->
                            <button onclick="addBien()"> Guardar</button>
                        </form>
                    </div>
                </div>
            <?php endforeach;?>
          </div>
        </div>
      </div>
      
      <div id="tabs-2" >
        <div class="colContenido" id="divResultadosBusqueda">
          <div class="tituloContenido card" style="justify-content: center;">
            <h5>Bienes guardados:</h5>
            <?php foreach ($data['misBienes'] as $key => $mibien) :?> 
              <div class="divider row" id="misBienes">
                <div class="imagenBien">
                  <img src="./resource/img/home.jpg" alt="" width="200px" height="200px">
                    </div>
                    <div class="datosBien" id="datosBien">
                        Dirección: <?php echo $mibien->direccion?><br>
                        <input type="hidden" name="idBien" id="idBien" value="<?php echo $mibien->id_bienes?>">
                        Ciudad: <?php echo $mibien->ciudad?><br>
                        Telefono: <?php echo $mibien->telefono?><br>
                        Codigo Postal: <?php echo $mibien->codigo_postal?><br>
                        Tipo: <?php echo $mibien->tipo?><br>
                        Precio: <?php echo "$".number_format($mibien->precio, 0, '.', ',');?><br>
                        <button  onclick="modalDelete()"> Eliminar</button>
                    </div>
              </div>
            <?php endforeach;?>
          </div>
        </div>
      </div>

      <div id="tabs-3" >

      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h5>¿Seguro que desea Eliminar el registro?</h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" onclick="modalCerrar()">Cerrar</button>
            <button type="button" class="btn btn-danger" onclick="destroy()">Eliminar</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <h5>!Se ha registrado un nuevo Bien, Felicidades...!</h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="modalCerrar()">Cerrar</button>
            <!-- <button type="button" class="btn btn-danger" onclick="destroy()">Eliminar</button> -->
          </div>
        </div>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="resource/js/ion.rangeSlider.min.js"></script>
    <script type="text/javascript" src="resource/js/materialize.min.js"></script>
    <script type="text/javascript" src="resource/js/index.js"></script>
    <script type="text/javascript" src="resource/js/buscador.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript" src="resource/js/funciones.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"></script> -->
    
    <script type="text/javascript">
      $( document ).ready(function() {
          $( "#tabs" ).tabs();
          
      });
     

      function buscarBienes(){
        var ciudad =$("#selectCiudad").val();
        var tipo =$("#selectTipo").val();
        // alert(ciudad +"-"+ tipo);
        $.ajax({
            type: "POST",
            url: 'Bienes/buscar',
            data: {
                    ciudad:ciudad,
                    tipo:tipo
            },
            success: function(response)
            {
              var datos = JSON.parse(response);
              console.log(datos);
              // $("#tabs-1").hide();
              $("#divResultadosBusqueda-1").hide();
              // $("#divResultadosBusqueda-1").load(" #divResultadosBusqueda-1");
              // $.each(datos, function (i, data) {
                for (let i = 0; i < datos.length; i++) {
                  content += '<div>';
                  content +=  '<input value='+datos[i]['Ciudad']+'>';
                  content += '</div>';
                }
                // content += '<div>';
            
                
              // });
              // return content;
              $("#tabs-3").innerHtTML= content;
            },
            error:function(error){
                console.error(error);
            }	
        });
      }

     
    </script>
  </body>
  </html> 
