<?php

class Bienes extends Controllers{
   
    public function __construct()
    {
        parent::__construct();

    }

    public function buscar()
    {
        $ciudad= $_POST['ciudad'];
        $tipo= $_POST['tipo'];

        $datos = file_get_contents("./resource/data-1.json");
        $datos = json_decode($datos);
        $newBienes = array();
        $idsBienes = array();
        if ($ciudad && $tipo) {
            foreach ($datos as $key => $dato) {
                if ($dato->Ciudad == $ciudad && $dato->Tipo == $tipo) {
                    $newBienes[]= $dato;
                    $idsBienes[]= $dato->Id;
                }
            }
        }
        if($ciudad){
            foreach ($datos as $key => $dato) {
                if ($dato->Ciudad == $ciudad && !in_array($dato->Id,$idsBienes)) {
                    $newBienes[]= $dato;
                    $idsBienes[]= $dato->Id;
                }
            }
        }
        if($tipo){
            foreach ($datos as $key => $dato) {
                if ($dato->Tipo == $tipo && !in_array($dato->Id,$idsBienes)) {
                    $newBienes[]= $dato;
                    $idsBienes[]= $dato->Id;
                }
            }
        }
        echo json_encode($newBienes);
    }

    public function store()
    {
        $direccion = $_POST['direccion'];
        $ciudad = $_POST['ciudad'];
        $telefono = $_POST['telefono'];
        $codigoPostal = $_POST['codigo_postal'];
        $tipo = $_POST['tipo'];
        $precio = substr($_POST['precio'], 1);
        $precio = explode(",", $precio);
        $precio = floatval($precio[0].$precio[1]);

        $data = $this->model->setBienes($direccion,$ciudad,$telefono,$codigoPostal,$tipo,$precio);
        echo json_encode($data);
        
    
    }

    public function destroy()
    {
        $id = $_POST['id_bienes'];
        $data = $this->model->deleteBien($id);
    }
    
}

?>