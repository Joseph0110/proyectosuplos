<?php 

class Home extends Controllers{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function home()
    {
            
        $datos = file_get_contents("./resource/data-1.json");
        $data['bienes'] = json_decode($datos);
        $data['ciudades'] = $this->getCiudades();
        $data['tipos'] = $this->getTipo();
        $data['misBienes'] = $this->getBienes();
    
        $this->views->getView($this, "home", $data);
    }

    private function getCiudades()
    {
        $ciudades= array();
        $datos = file_get_contents("./resource/data-1.json");
        $bienes = json_decode($datos);

       foreach ($bienes as $key => $bien) {
            $ciudades[]=$bien->Ciudad;
       }

       return $ciudades = array_values(array_unique($ciudades));
      
    }

    private function getTipo()
    {
        $tipos= array();
        $datos = file_get_contents("./resource/data-1.json");
        $bienes = json_decode($datos);

       foreach ($bienes as $key => $bien) {
            $tipos[]=$bien->Tipo;
       }

       return $tipos = array_values(array_unique($tipos));
      
    }

    private function getBienes()
    {
        $datos = $this->model->getBienes();
        $misBienes=array();
        foreach ($datos as $key => $dato) {
            $misBienes[]= (object)$dato;
        }
    
        return $misBienes;
    }
}


?>