function modalDelete(){
    $("#deleteModal").show();
  }
  function modalCerrar(){
    $("#addModal").hide();
    $("#deleteModal").hide();
  }

  function destroy(){
    var id =$("#idBien").val();
    $.ajax({
        type: "POST",
        url: 'Bienes/destroy',
        data: {id_bienes:id},
        success: function(response)
        {
          $("#tabs-2").load(" #tabs-2");
          $("#deleteModal").hide();
        },
        error:function(error){
            console.error(error);
        }
    });
  }

  function addBien(){
    var direccion =$("#direccion").val();
    var ciudad =$("#ciudad").val();
    var telefono =$("#telefono").val();
    var codigoPostal =$("#codigoPosta").val();
    var tipo =$("#tipo").val();
    var precio =$("#precio").val();
    $.ajax({
        type: "POST",
        url: 'Bienes/store',
        data: {
                direccion:direccion,
                ciudad:ciudad,
                telefono:telefono,
                codigo_postal:codigoPostal,
                tipo:tipo,
                precio:precio
        },
        success: function(response)
        {
          $("#tabs-2").load(" #tabs-2");
          $("#addModal").show();
        },
        error:function(error){
            console.error(error);
        }	
    });
  }
  
  // function buscarBienes(){
  //   var ciudad =$("#selectCiudad").val();
  //   var tipo =$("#selectTipo").val();
  //   // alert(ciudad +"-"+ tipo);
  //   $.ajax({
  //       type: "POST",
  //       url: 'Bienes/buscar',
  //       data: {
  //               ciudad:ciudad,
  //               tipo:tipo
  //       },
  //       success: function(response)
  //       {
  //         var datos = JSON.parse(response);
  //         console.log(datos);
  //         $("#tabs-1").hide();
  //         $("#divResultadosBusqueda-1").load(" #divResultadosBusqueda-1");
  //         $.each(datos, function (i, data) {
  //           // content += '<div>'
  //           //   content += '<div class="colContenido" id="divResultadosBusqueda">'
  //           //     content += '<div class="tituloContenido card" style="justify-content: center;">'
  //           //       content += '<h5>Resultados de la búsqueda:</h5>'
  //           //         content += '<div class="divider row">'
  //           //           content += '<div class="imagenBien">'
  //           //             content += '<img src="./resource/img/home.jpg" alt="" width="200px" height="200px">'
  //           //           content += '</div>'
  //           //           content += ''
  //           //         content += '</div>'
  //           //     content += '</div>'
  //           //   content += '</div>'
  //           // content += '</div>'
  //         });
  //         return content;
          
  //       },
  //       error:function(error){
  //           console.error(error);
  //       }	
  //   });
  // }
